export const ADD_NOTE = "ADD_NOTE";

export interface addNoteAction {
	type: typeof ADD_NOTE;
	payload: string;
}
