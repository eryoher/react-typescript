import { addNoteAction, ADD_NOTE } from "../constants/ActionsTypes";

export const addNote = (note: string): addNoteAction => ({
	type: ADD_NOTE,
	payload: note,
});
