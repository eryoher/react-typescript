import { addNoteAction, ADD_NOTE } from "./../constants/ActionsTypes";

export interface NotesState {
	notes: string[];
}

const initialState = {
	notes: [],
};

export function notesReducer(state: NotesState = initialState, action: addNoteAction) {
	switch (action.type) {
		case ADD_NOTE: {
			return { ...state, notes: [...state.notes, action.payload] };
		}
		default:
			return state;
	}
}
