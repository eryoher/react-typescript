import React from "react";
import { NewNoteInput } from "./NewNoteInput";
import { useDispatch, useSelector } from "react-redux";
import { addNote } from "./actions/Notes";
import { RootStore } from "./store";

function App() {
	const notesState = useSelector((state: RootStore) => state.notes);
	const dispatch = useDispatch();

	const onAddNote = (note: string) => {
		dispatch(addNote(note));
	};

	return (
		<>
			<NewNoteInput addNote={onAddNote} />
			<hr />
			<ul>
				{notesState.notes.map((note) => {
					return <li key={note}>{note}</li>;
				})}
			</ul>
		</>
	);
}

export default App;
